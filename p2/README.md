# LIS4368 Advanced Web Application Development

## Sandro Perez

### Project 2 Requirements:

*In project 2, we have:*

1. Edit and delete to customer table through form
2. Code and Compile each corresponding Java and JSP file to accept entries
3. Display customers
4. Chapter questions

#### README.md file should include the following items:

* Screenshots of pre-, post-valid user form entry, as well as MySQL customer table entry 

#### Git commands w/short descriptions:
1. git init : Create a local new repository 
2. git add : Add one or more files to a staging
3. git status : List the files you've chnaged and those you still need to add or commit
4. git push : Send chnages to the master branch of your remote repository 
5. git pull : Fetch and merge changes on the remote server to your working directory
6. git commit :  Commit changes to head but not yet to the remote repository 
7. git remote -v : List all currently configured remote repositories

#### Assignment Screenshots:

| *Screenshot of the P2 Form Validation*:                           | *Screenshot of the P2 Completed Form*:                | *Screenshot of Display Table*:                       |   |   |
|-------------------------------------------------------------------|-------------------------------------------------------|------------------------------------------------------|---|---|
| ![P2 Form Validation](img/filled.png "Customer Form Validation ") | ![P2 Completed Form](img/submit.png "Completed Form") | ![P2 Display Table](img/display.png "Display Table") |   |   |
| *Screenshot of the Delete Confirmation*:                          | *Screenshot of mysql tables*:                         |                                                      |   |   |
| ![P2 Delete Feature](img/confirm.png "Delete Confirmation")       | ![P2 mysql tables](img/sql.png "mysql tables")        |                                                      |   |   |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
