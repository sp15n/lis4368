# LIS4368 Advanced Web Application Development

## Sandro Perez

### Assignment 5 Requirements:

*In assignment 5, we have:*

1. Built a server-side validation form
2. Code and Compile each corresponding Java and JSP file to accept entries
3. Provide a5 files/screenshots

#### README.md file should include the following items:

* Screenshot of the form validation, completed form, and proof of data insert. 

#### Git commands w/short descriptions:
1. git init : Create a local new repository 
2. git add : Add one or more files to a staging
3. git status : List the files you've chnaged and those you still need to add or commit
4. git push : Send chnages to the master branch of your remote repository 
5. git pull : Fetch and merge changes on the remote server to your working directory
6. git commit :  Commit changes to head but not yet to the remote repository 
7. git remote -v : List all currently configured remote repositories

#### Assignment Screenshots:

| *Screenshot of the A5 Form Validation*:                          | *Screenshot of the A5 Completed Form*:              | *Screenshot of the A5 Completed Form*:                                |   |   |
|------------------------------------------------------------------|-----------------------------------------------------|-----------------------------------------------------------------------|---|---|
| ![A5 Form Validation](img/empty.png "Customer Form Validation ") | ![A5 Completed Form](img/done.png "Completed Form") | ![A5 Data Insert after Form](img/data.png "Proof of Customer Insert") |   |   |
|                                                                  |                                                     |                                                                       |   |   |
|                                                                  |                                                     |                                                                       |   |   |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
