import java.util.*;

public class Ascii {
    public static void main(String[] args) {
        // initiailize variables
        int num = 0;
        boolean isValidNum = false;
        Scanner sc = new Scanner(System.in);

        System.out.println("Printing character A-Z as ASCII values");
        for (char character = 'A'; character <= 'Z'; character++) {
            System.out.printf("Character %c has ascii value %c\n", character, (int) character);
        }
        // Or, typecasting
        System.out.println("\nPrinting ASCII values 48-122 as characters");
        for (num = 48; num <= 122; num++) {
            System.out.printf("ASCII value %d has character value %c\n", num, (char) num);
        }
        // allow user input
        System.out.println("\nAllowing user ASCII value input");

        while (isValidNum == false) {
            // check num double
            System.out.print("Please enter ASCII value(32-127): ");
            if (sc.hasNextInt()) {
                num = sc.nextInt();
                isValidNum = true;
            } else {
                System.out.println("Invalid integer--ASCII value must be a number.\n");
            }
            sc.nextLine(); // discard any other data enterd on line

            // num data true, check num range
            if (isValidNum == true && num < 32 || num > 127) {
                System.out.println("ASCII value must be > = 32 and < = 127.\n");
                isValidNum = false;
            }
            if (isValidNum == true) {
                System.out.println();

                // dipslay result
                System.out.printf("ASCII value %d has character value %c\n", num, ((char) num));
            }
        } // end of while
    }// end of main
}// end of class