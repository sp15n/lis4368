# LIS4368 Advanced Web Application Development

## Sandro Perez

### Assignment 4 Requirements:

*In assignment 4, we have:*

1. Built a server-side validation form
2. Code and Compile each corresponding Java and JSP file to accept entries
3. Provide a4 files/screenshots

#### README.md file should include the following items:

* Screenshot of the empty form, form with error messages, and submited form results. 


#### Assignment Screenshots:

*Screenshot of the A4 Empty Form*:

![A4 Empty Form](img/empty.png "Server-side Validation Form Empty") 

*Screenshot of the A4 Submitted Form Results*:

![A4 Empty Form](img/results.png "Server-side Validation Form Submitted Results") 

*Customer.java and CustomerServlet.java files*
[A4 CustomerServlet](doc/CustomerServlet.java "CustomerServlet.java")
[A4 Customer](doc/Customer.java "Customer.java")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
