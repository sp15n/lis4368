# LIS4368 Advanced Web Application Development

## Sandro Perez

### Assignment 3 Requirements:

*In assignment 3, we have:*

1. Design an Entity Relation Diagram 
2. Inport and include data (at least 10 records per table)
3. Forward engineer and provide the mwb and sql files
4. Provide a png file of the ERD 

#### README.md file should include the following items:

* Screenshot of the Entity Relation Diagram as a .png file
* Provide the .mwb and .sql for the Database under the docs folder


#### Assignment Screenshots:

*Screenshot of the A3 ERD*:

![A3 ERD](img/a3.png "ERD based upon A3 Requirements") 

*A3 Docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
