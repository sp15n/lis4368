
# LIS4368 Advance Web Application Development

## Sandro Perez

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Setting up a Distributed Version Control with Bitbucket
    - Successfully run a Hello.java program 
    - Download and run AMPPs
    - Run and view http://localhost:9999
    - Run and view personal class site http://localhost:9999/lis4368

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Tomcat Compile First Servlets
    - Connect to mySQL database with query
    - Provide git command descriptions

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a Entity Relationship Diagram
    - Insert Data into Each Table (min. 10 per table)
    - Save the ERD as a PNG file
    - Save the Database as a .sql and .mwb file

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Update main page banner images and links
    - Client Side Validation
    - Showcase Validations

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server Side Validation
    - Showcase Validations
    - Provide A4 form screenshots
    
6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server Side/Database Validation
    - Showcasing Validations
    - Provide A5 form/data insert screenshots  

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Edit and delete to customer table through form
    - Code and Compile each corresponding Java and JSP file to accept entries
    - Display customers
    - Chapter questions

