
# LIS4368 Advanced Web Application Development

## Sandro Perez

### Assignment 1 Requirements:

*In assignment 1, we have:*

1. Set up a Distributed Version Control with Git and Bitbucket
2. Researched and provided a description of 7 git commands
3. Downloaded and installed JDK
4. Downloaded and run AMPPs 
5. Programed a Hello-World Java Program
6. Compiled and ran the Hello-World Java Program
7. Downloaded and ran http://localhost:9999
8. Modified and ran class webpage under http://localhost:9999/lis4368

#### README.md file should include the following items:

* Screenshot of properly compiling and running the Hello-World Java program
* 7 git commands and a short description of each
* Screenshots of running AMPPs 
* Screenshot of running http://localhost:9999
* Screenshot of running http://localhost:9999/lis4368 


#### Git commands w/short descriptions:
1. git init : Create a local new repository 
2. git add : Add one or more files to a staging
3. git status : List the files you've chnaged and those you still need to add or commit
4. git push : Send chnages to the master branch of your remote repository 
5. git pull : Fetch and merge changes on the remote server to your working directory
6. git commit :  Commit changes to head but not yet to the remote repository 
7. git remote -v : List all currently configured remote repositories

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running the AMPPs application*:

![Running AMPPS](img/runningampps.png)

*Screenshot of running http://localhost:9999:

![Running http://localhost:9999](img/runninglocalhost.png)

*Screenshot of running http://localhost:9999/lis4368:

![Running http://localhost:9999/lis4368](img/runningcoursesite.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
