# LIS4368 Advanced Web Application Development

## Sandro Perez

### Assignment 2 Requirements:

*In assignment 2, we have:*

1. Configure mySQL Database
2. Configure index file to connect to database using AMPPS username/password
3. Chapter Questions
4. Use the QueryBook.html and have functioning results

#### README.md file should include the following items:

* Screenshot of hello, sayhello, index, and query
* git commands with short descriptions

#### Git commands w/short descriptions:
1. git init : Create a local new repository 
2. git add : Add one or more files to a staging
3. git status : List the files you've chnaged and those you still need to add or commit
4. git push : Send chnages to the master branch of your remote repository 
5. git pull : Fetch and merge changes on the remote server to your working directory
6. git commit :  Commit changes to head but not yet to the remote repository 
7. git remote -v : List all currently configured remote repositories

#### Assignment Screenshots:

*Screenshot of Index http://localhost:9999/hello*:

![Screenshot of Index/Hello](img/hello.png)

*Screenshot of Say Hello http://localhost:9999/hello/sayhello*:

![JDK Installation Screenshot](img/sayhello.png)

*Screenshot of Say Hi http://localhost:9999/hello/sayhi*:

![JDK Installation Screenshot](img/sayhello.png)

*Screenshot of QueryBook Unchecked http://localhost:9999/hello/querybook*:

![Running AMPPS](img/queryunchecked.png)

*Screenshot of QueryBook Checked http://localhost:9999/hello/querybook*:

![Running http://localhost:9999](img/querychecked.png)

*Screenshot of QueryBook Results http://localhost:9999/hello/querybook*:

![Running http://localhost:9999](img/queryresult.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
