# LIS4368 Advanced Web Application Development

## Sandro Perez

### Project 1 Requirements:

*In project 1, we have:*

1. Formed Tables Addition
2. Included requirements in basic client side validation
3. Inputted the Validation Text
4. Answered the Chapter questions

#### README.md file should include the following items:

* Screenshot of banner running with links
* Screenshots of client-side validation empty and filled in


#### Assignment Screenshots:

*Screenshot of main banner*:

![Main Course Banner](img/banner.png "Banner change on main Index") 

*Screenshot of empty client-side validation*:
![Empty Validation](img/empty.png "Screenshot of empty client-side validation") 

*Screenshot of filled in client-side validation*:
![Filled Validation](img/filled.png "Screenshot of filled in client-side validation") 



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
